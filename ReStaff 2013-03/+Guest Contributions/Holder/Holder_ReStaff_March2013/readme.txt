Name: Holder

Terms of Use: Commercial/Non-Commercial/Contact me for Commercial Usage.
-You may download and store these images you plan to use as materials for the creation of a project.
-You may redistribute via projects and script demos (i.e. which make use of the graphics).
-You may edit upon these images, named credit for images at any level of edit.
-Intended for use with the RPG Maker engine (any version) only.
-Do not redistribute (content is available across different communities, please link to my topics/galleries/blog).
-If you would like to advertise these elsewhere, you may use a few (around 3) thumbnail animated previews as examples.
-Do not remove the credits section (to ensure that this information isn�t lost in time).
-Do not remove the credits section and redistribute (both reasons stated above).
-Not for use within outside accessible archives or complied into a downloadable pack.
-If my work is used for Commercial purposes with RPG Maker (any version) or any other program I hold/have no connection nor have I made or will make any direct profit from that.
-Free to work upon these images, you must still credit the original and not remove any text that may be within the image which state the original details (i.e. Created by details, Based on credits, Date originally created). Inclusion of additional text within the credits section is fine.
-The user retains all responsibility, I hold no connection with any party and they assume all responsibility for any given situation regarding conflict, resolution or judgement.


Contact Info: 
-Website URL:	http://animatedbattlers.wordpress.com/
-MSN:		dark_holder
-Skype:		dark_holder
-Twitter:	@Holder_AB

Details: 
Holders Animated Battlers, for use with animated battle scripts across all RPG Maker versions;

*RPG Maker VX Ace RGSS3 Compatible Scripts:

	-Victor Engine � Animated Battle
		http://victorscripts.wordpress.com/rpg-maker-vx-ace/battle-scripts/animated-battle/
	
	-Kreads Animated Battlers
		http://grimoirecastle.wordpress.com/rgss3-scripts/graphics-enhancements/animated-battlers/

	-Fomar0153 � Animated Battlers
		http://cobbtocs.co.uk/wp/?p=70
	
*RPG Maker VX RGSS2 Compatible Scripts:

	-Minkoff�s Animated Battlers Enhanced VX
		http://houseslasher.com/index.php?showtopic=46

	-Animated Battler Graphic
		http://forums.rpgmakerweb.com/index.php?/topic/713-animated-battler-graphic-script-demo
	
*RPG Maker XP RGSS Compatible Scripts:

	-Minkoff�s Animated Battlers Enhanced
		http://houseslasher.com/index.php?showtopic=173

	-Moonpearl�s Animated Battlers
		http://moonpearl-gm.blogspot.fr/2012/11/monnpearls-animated-battlers.html

*If there's a script missing from any versions please contact me so I can alter the list in the future.

*Notes: You can find more of my Animated Battlers at the following topic;
http://forums.rpgmakerweb.com/index.php?/topic/469-holders-animated-battlers/