Name: Kairi Blodeuyn Sawler,(aka Blodeuyn)
 
Terms of Use: Free for Non-Commercial use (with credit given); Commercial use by contacting me, not for free, however. Interested people may purchase the songs, or commercial licenses from me, and may get more information on that with a simple PM here on the site.
 
Contact Info: 
Contact Blodeuyn on RPGMaker Forums, or at my Website
 
Details: 
My resource is music, lots of little repeatable tunes that work well with RPG Maker games. There are currently 16 songs between 30 seconds and 5 minutes in length. They are available for listening on my Soundcloud
 