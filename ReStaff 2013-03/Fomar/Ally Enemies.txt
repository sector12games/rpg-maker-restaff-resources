=begin
Ally Enemies
by Fomar0153
Version 1.0
----------------------
Notes
----------------------
Allows you to add enemies to the party.
They do not gain levels and cannot change equipment.
----------------------
Instructions
----------------------
To add an ally:
$game_party.add_ally(enemy_id)

To remove an ally:
$game_party.remove_ally(enemy_id)

To change an ally's equipment
$game_party.get_ally(enemy_id).equips[0] = $data_weapons[x]
$game_party.get_ally(enemy_id).equips[y] = $data_armors[z]

Notetags
<character_name str>
where str is the name of character sheet

<character_index x>
where x is the index of the character on the sheet

<face_name str>
where str is the name of face sheet

<face_index x>
where x is the index of the face on the sheet

<level x>
Level to be diplayed.

<class x>
Class name to display.

<description str>
where str is the description to be displayed.

<nickname str>
where str is the nickname to be displayed.
----------------------
Known bugs
----------------------
None
=end

class Game_Ally < Game_Enemy
  #--------------------------------------------------------------------------
  # * Public Instance Variables
  #--------------------------------------------------------------------------
  attr_reader   :character_name
  attr_reader   :character_index
  attr_reader   :face_name
  attr_reader   :face_index
  attr_reader   :class_id
  attr_reader   :level
  attr_reader   :last_skill
  attr_accessor :equips
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  def initialize(ally_id)
    super(0,ally_id)
    @character_name = enemy.character_name
    @character_index = enemy.character_index
    @face_name = enemy.face_name
    @face_index = enemy.face_index
    @class_id = enemy.class_id
    @level = enemy.level
    @last_skill = Game_BaseItem.new
    @equips = [nil,nil,nil,nil,nil]
  end
  #--------------------------------------------------------------------------
  # * Index
  #--------------------------------------------------------------------------
  def index
    $game_party.members.index(self)
  end
  #--------------------------------------------------------------------------
  # * Id
  #--------------------------------------------------------------------------
  def id
    $data_actors.size + enemy.id
  end
  #--------------------------------------------------------------------------
  # * Determine if Enemy
  #--------------------------------------------------------------------------
  def enemy?
    return false
  end
  #--------------------------------------------------------------------------
  # * Get Allied Units
  #--------------------------------------------------------------------------
  def friends_unit
    $game_party
  end
  #--------------------------------------------------------------------------
  # * Get Enemy Units
  #--------------------------------------------------------------------------
  def opponents_unit
    $game_troop
  end
  #--------------------------------------------------------------------------
  # * Determine if Command is Inputable
  #--------------------------------------------------------------------------
  def inputable?
    false
  end
  #--------------------------------------------------------------------------
  # * Use Sprites?
  #--------------------------------------------------------------------------
  def use_sprite?
    return false
  end
  #--------------------------------------------------------------------------
  # * To Next Command Input
  #--------------------------------------------------------------------------
  def next_command
    return false
  end
  #--------------------------------------------------------------------------
  # * To Previous Command Input
  #--------------------------------------------------------------------------
  def prior_command
    return false
  end
  #--------------------------------------------------------------------------
  # * Get Total EXP Required for Rising to Specified Level
  #--------------------------------------------------------------------------
  def exp_for_level(level)
    0
  end
  #--------------------------------------------------------------------------
  # * Get Experience
  #--------------------------------------------------------------------------
  def exp
    0
  end
  #--------------------------------------------------------------------------
  # * Get Minimum EXP for Current Level
  #--------------------------------------------------------------------------
  def current_level_exp
    0
  end
  #--------------------------------------------------------------------------
  # * Get EXP for Next Level
  #--------------------------------------------------------------------------
  def next_level_exp
    0
  end
  #--------------------------------------------------------------------------
  # * Maximum Level
  #--------------------------------------------------------------------------
  def max_level
    @level
  end
  #--------------------------------------------------------------------------
  # * Determine Maximum Level
  #--------------------------------------------------------------------------
  def max_level?
    @level >= max_level
  end
  #--------------------------------------------------------------------------
  # * Convert from Equipment Type to List of Slot IDs
  #--------------------------------------------------------------------------
  def slot_list(etype_id)
    result = []
    equip_slots.each_with_index {|e, i| result.push(i) if e == etype_id }
    result
  end
  #--------------------------------------------------------------------------
  # * Convert from Equipment Type to Slot ID (Empty Take Precedence)
  #--------------------------------------------------------------------------
  def empty_slot(etype_id)
  end
  #--------------------------------------------------------------------------
  # * Get Equipment Slot Array
  #--------------------------------------------------------------------------
  def equip_slots
    return [0,1,2,3,4]
  end
  #--------------------------------------------------------------------------
  # * Get Equipped Item Object Array
  #--------------------------------------------------------------------------
  def equips
    @equips
  end
  #--------------------------------------------------------------------------
  # * Determine if Equipment Change Possible
  #     slot_id:  Equipment slot ID
  #--------------------------------------------------------------------------
  def equip_change_ok?(slot_id)
    return false
  end
  #--------------------------------------------------------------------------
  # * Change Equipment
  #     slot_id:  Equipment slot ID
  #     item:    Weapon/armor (remove equipment if nil)
  #--------------------------------------------------------------------------
  def change_equip(slot_id, item)
  end
  #--------------------------------------------------------------------------
  # * Forcibly Change Equipment
  #     slot_id:  Equipment slot ID
  #     item:     Weapon/armor (remove equipment if nil)
  #--------------------------------------------------------------------------
  def force_change_equip(slot_id, item)
  end
  #--------------------------------------------------------------------------
  # * Remove All Equipment
  #--------------------------------------------------------------------------
  def clear_equipments
  end
  #--------------------------------------------------------------------------
  # * Ultimate Equipment
  #--------------------------------------------------------------------------
  def optimize_equipments
  end
  #--------------------------------------------------------------------------
  # * Determine if Skill-Required Weapon Is Equipped
  #--------------------------------------------------------------------------
  def skill_wtype_ok?(skill)
    return true
  end
  #--------------------------------------------------------------------------
  # * Determine Battle Members
  #--------------------------------------------------------------------------
  def battle_member?
    $game_party.battle_members.include?(self)
  end
  #--------------------------------------------------------------------------
  # * Get Class Object
  #--------------------------------------------------------------------------
  def class
    $data_classes[@class_id]
  end
  #--------------------------------------------------------------------------
  # * Get Skill Object Array
  #--------------------------------------------------------------------------
  def skills
    []
  end
  #--------------------------------------------------------------------------
  # * Get Array of Currently Usable Skills
  #--------------------------------------------------------------------------
  def usable_skills
    skills.select {|skill| usable?(skill) }
  end
  #--------------------------------------------------------------------------
  # * Level Up
  #--------------------------------------------------------------------------
  def level_up
  end
  #--------------------------------------------------------------------------
  # * Level Down
  #--------------------------------------------------------------------------
  def level_down
  end
  #--------------------------------------------------------------------------
  # * Show Level Up Message
  #     new_skills : Array of newly learned skills
  #--------------------------------------------------------------------------
  def display_level_up(new_skills)
  end
  #--------------------------------------------------------------------------
  # * Get EXP (Account for Experience Rate)
  #--------------------------------------------------------------------------
  def gain_exp(exp)
  end
  #--------------------------------------------------------------------------
  # * Calculate Final EXP Rate
  #--------------------------------------------------------------------------
  def final_exp_rate
    0
  end
  #--------------------------------------------------------------------------
  # * Change Level
  #     show : Level up display flag
  #--------------------------------------------------------------------------
  def change_level(level, show)
  end
  #--------------------------------------------------------------------------
  # * Get Description
  #--------------------------------------------------------------------------
  def description
    enemy.description
  end
  #--------------------------------------------------------------------------
  # * Get Nickname
  #--------------------------------------------------------------------------
  def nickname
    enemy.nickname
  end
  #--------------------------------------------------------------------------
  # * Change Graphics
  #--------------------------------------------------------------------------
  def set_graphic(character_name, character_index, face_name, face_index)
    @character_name = character_name
    @character_index = character_index
    @face_name = face_name
    @face_index = face_index
  end
  #--------------------------------------------------------------------------
  # * Execute Damage Effect
  #--------------------------------------------------------------------------
  def perform_damage_effect
    $game_troop.screen.start_shake(5, 5, 10)
    @sprite_effect_type = :blink
    Sound.play_actor_damage
  end
  #--------------------------------------------------------------------------
  # * Execute Collapse Effect
  #--------------------------------------------------------------------------
  def perform_collapse_effect
    if $game_party.in_battle
      @sprite_effect_type = :collapse
      Sound.play_actor_collapse
    end
  end
  #--------------------------------------------------------------------------
  # * Processing Performed When Player Takes 1 Step
  #--------------------------------------------------------------------------
  def on_player_walk
    @result.clear
    check_floor_effect
    if $game_player.normal_walk?
      turn_end_on_map
      states.each {|state| update_state_steps(state) }
      show_added_states
      show_removed_states
    end
  end
  #--------------------------------------------------------------------------
  # * Update Step Count for State
  #--------------------------------------------------------------------------
  def update_state_steps(state)
    if state.remove_by_walking
      @state_steps[state.id] -= 1 if @state_steps[state.id] > 0
      remove_state(state.id) if @state_steps[state.id] == 0
    end
  end
  #--------------------------------------------------------------------------
  # * Show Added State
  #--------------------------------------------------------------------------
  def show_added_states
    @result.added_state_objects.each do |state|
      $game_message.add(name + state.message1) unless state.message1.empty?
    end
  end
  #--------------------------------------------------------------------------
  # * Show Removed State
  #--------------------------------------------------------------------------
  def show_removed_states
    @result.removed_state_objects.each do |state|
      $game_message.add(name + state.message4) unless state.message4.empty?
    end
  end
  #--------------------------------------------------------------------------
  # * Number of Steps Regarded as One Turn in Battle
  #--------------------------------------------------------------------------
  def steps_for_turn
    return 20
  end
  #--------------------------------------------------------------------------
  # * End of Turn Processing on Map Screen
  #--------------------------------------------------------------------------
  def turn_end_on_map
    if $game_party.steps % steps_for_turn == 0
      on_turn_end
      perform_map_damage_effect if @result.hp_damage > 0
    end
  end
  #--------------------------------------------------------------------------
  # * Determine Floor Effect
  #--------------------------------------------------------------------------
  def check_floor_effect
    execute_floor_damage if $game_player.on_damage_floor?
  end
  #--------------------------------------------------------------------------
  # * Floor Damage Processing
  #--------------------------------------------------------------------------
  def execute_floor_damage
    damage = (basic_floor_damage * fdr).to_i
    self.hp -= [damage, max_floor_damage].min
    perform_map_damage_effect if damage > 0
  end
  #--------------------------------------------------------------------------
  # * Get Base Value for Floor Damage
  #--------------------------------------------------------------------------
  def basic_floor_damage
    return 10
  end
  #--------------------------------------------------------------------------
  # * Get Maximum Value for Floor Damage
  #--------------------------------------------------------------------------
  def max_floor_damage
    $data_system.opt_floor_death ? hp : [hp - 1, 0].max
  end
  #--------------------------------------------------------------------------
  # * Execute Damage Effect on Map
  #--------------------------------------------------------------------------
  def perform_map_damage_effect
    $game_map.screen.start_flash_for_damage
  end
end

class Game_Party < Game_Unit
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  alias ally_initialize initialize
  def initialize
    ally_initialize
    @allies = []
    @allies_data = []
  end
  #--------------------------------------------------------------------------
  # * Get All Members
  #--------------------------------------------------------------------------
  alias ally_all_members all_members
  def all_members
    ally_all_members + @allies.collect {|id| get_ally(id) }
  end
  #--------------------------------------------------------------------------
  # * Add an Ally
  #--------------------------------------------------------------------------
  def add_ally(ally_id)
    @allies.push(ally_id) unless @allies.include?(ally_id)
    $game_player.refresh
    $game_map.need_refresh = true
  end
  #--------------------------------------------------------------------------
  # * Remove Ally
  #--------------------------------------------------------------------------
  def remove_ally(ally_id)
    @allies.delete(ally_id)
    $game_player.refresh
    $game_map.need_refresh = true
  end
  #--------------------------------------------------------------------------
  # * Get Ally
  #--------------------------------------------------------------------------
  def get_ally(ally_id)
    return nil unless $data_enemies[ally_id]
    @allies_data[ally_id] ||= Game_Ally.new(ally_id)
  end
  #--------------------------------------------------------------------------
  # * Change Order
  #--------------------------------------------------------------------------
  alias ally_swap_order swap_order
  def swap_order(index1, index2)
    if index1 > @actors.size - 1 and index2 > @actors.size - 1
      index1 -= @actors.size
      index2 -= @actors.size
      @allies[index1], @allies[index2] = @allies[index2], @allies[index1]
      $game_player.refresh
    end
    if index1 < @actors.size and index2 < @actors.size
      ally_swap_order(index1, index2)
    end
  end
end

class Game_Actors
  #--------------------------------------------------------------------------
  # * Get Actor
  #--------------------------------------------------------------------------
  def [](actor_id)
    if actor_id > $data_actors.size
      return $game_party.get_ally(actor_id - $data_actors.size)
    end
    return nil unless $data_actors[actor_id]
    @data[actor_id] ||= Game_Actor.new(actor_id)
  end
end

class RPG::Enemy < RPG::BaseItem
  #--------------------------------------------------------------------------
  # * Get Character Name
  #--------------------------------------------------------------------------
  def character_name
    if @character_name.nil?
      if @note =~ /<character_name (.*)>/i
        @character_name = $1
      else
        @character_name = ""
      end
    end
    @character_name
  end
  #--------------------------------------------------------------------------
  # * Get Character Index
  #--------------------------------------------------------------------------
  def character_index
    if @character_index.nil?
      if @note =~ /<character_index (.*)>/i
        @character_index = $1.to_i
      else
        @character_index = 0
      end
    end
    @character_index
  end
  #--------------------------------------------------------------------------
  # * Get face Name
  #--------------------------------------------------------------------------
  def face_name
    if @face_name.nil?
      if @note =~ /<face_name (.*)>/i
        @face_name = $1
      else
        @face_name = ""
      end
    end
    @face_name
  end
  #--------------------------------------------------------------------------
  # * Get face Index
  #--------------------------------------------------------------------------
  def face_index
    if @face_index.nil?
      if @note =~ /<face_index (.*)>/i
        @face_index = $1.to_i
      else
        @face_index = 0
      end
    end
    @face_index
  end
  #--------------------------------------------------------------------------
  # * Get Class Id
  #--------------------------------------------------------------------------
  def class_id
    if @class_id.nil?
      if @note =~ /<class_id (.*)>/i
        @class_id = $1.to_i
      else
        @class_id = 1
      end
    end
    @class_id
  end
  #--------------------------------------------------------------------------
  # * Get face Index
  #--------------------------------------------------------------------------
  def level
    if @level.nil?
      if @note =~ /<level (.*)>/i
        @level = $1.to_i
      else
        @level = 1
      end
    end
    @level
  end
  #--------------------------------------------------------------------------
  # * Get Description
  #--------------------------------------------------------------------------
  def description
    if @description.nil?
      if @note =~ /<description (.*)>/i
        @description = $1
      else
        @description = ""
      end
    end
    @description
  end
  #--------------------------------------------------------------------------
  # * Get Description
  #--------------------------------------------------------------------------
  def nickname
    if @nickname.nil?
      if @note =~ /<nickname (.*)>/i
        @nickname = $1
      else
        @nickname = ""
      end
    end
    @nickname
  end
end