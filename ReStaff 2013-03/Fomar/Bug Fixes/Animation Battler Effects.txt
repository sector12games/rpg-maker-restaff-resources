=begin
Animation Battler Effects
by Fomar0153
Version 1.1
----------------------
Notes
----------------------
Makes animations more interesting by letting you 
modify a targets battler as part of the animation,.
----------------------
Instructions
----------------------
Basically you need to "nametag" the animations (how very RPG Maker XP)
the guidelines for the tag are below.

<effect effect,start_frame,length,reset_on_end>
e.g.
<effect hop,1,6,true>

effect can be one of: flatten, zoomin, zoomout, bulge or hop
More effects can be added later.

start_frame refers to the frame this effect is applied to the battler.
length refers to frames of the animation not actual game frames
reset_on_end when true will reset the battler as soon as the battler effect
is finished. When false it will reset the battler when the whole animation is
finished.
----------------------
Change Log
----------------------
1.0 -> 1.1 : Fixed a game crashing bug.
----------------------
Known bugs
----------------------
None
=end
module AnimationEffects
  
  def self.applyeffect(sprite, effect, frame, length = 1)
    case effect
    when "flatten"
      case frame
      when -1 # end
        sprite.zoom_y = 1.0
        sprite.zoom_x = 1.0
      else
        sprite.zoom_y = [1.0 - (frame / length.to_f),0.1].max
        sprite.zoom_x = [1.0 + 0.2 * (frame / length.to_f),1.2].min
      end # flatten
    when "zoomin"
      case frame
      when -1 # end
        sprite.zoom_y = 1.0
        sprite.zoom_x = 1.0
      else
        sprite.zoom_x = [1.0 + (frame / length.to_f),2].min
        sprite.zoom_y = [1.0 + (frame / length.to_f),2].min
      end # zoomin
    when "zoomout"
      case frame
      when -1 # end
        sprite.zoom_y = 1.0
        sprite.zoom_x = 1.0
      else
        sprite.zoom_x = [1.0 - (frame / length.to_f),0.1].max
        sprite.zoom_y = [1.0 - (frame / length.to_f),0.1].max
      end # zoomout
    when "bulge"
      case frame
      when -1 # end
        sprite.zoom_y = 1.0
        sprite.zoom_x = 1.0
      else
        if frame < length / 2
          rate = 2 * (frame / length.to_f)
          sprite.zoom_x = [1.0 - 0.5 * rate,0.5].max
          sprite.zoom_y = [1.0 + 0.5 * rate,1.5].min
        else
          rate = ((frame-length / 2) / (length.to_f-length / 2))
          sprite.zoom_x = [0.5 + 0.5 * rate,1.0].min
          sprite.zoom_y = [1.5 - 0.5 * (frame / length.to_f),1.0].max
        end
      end # bulge
    when "hop"
      case frame
      when -1 # end
        sprite.y = sprite.animation_settings['y']
      when 0
        sprite.animation_settings['y'] = sprite.y
        sprite.y -= 200 / length.to_f
      else
        if frame < length / 2
          sprite.y -= 200 / length.to_f
        else
          sprite.y += 200 / length.to_f
        end
      end # hop
    end
  end
  
  def self.endeffect(sprite, effect)
    self.applyeffect(sprite, effect, -1)
    sprite.animation_settings = {}
  end
  
end


class Sprite_Base < Sprite
  #--------------------------------------------------------------------------
  # * Public Instance Variables
  #--------------------------------------------------------------------------
  attr_accessor :animation_settings
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  alias se_initialize initialize
  def initialize(viewport = nil)
    se_initialize(viewport)
    @animation_settings = {}
  end
  #--------------------------------------------------------------------------
  # * Update Animation
  #--------------------------------------------------------------------------
  alias se_update_animation update_animation
  def update_animation
    return unless animation?
    if @animation.effect.size > 0
      frame_index = @animation.frame_max - ((@ani_duration + @ani_rate - 2) / @ani_rate)
      if frame_index >= @animation.effect[1].to_i and frame_index < @animation.effect[1].to_i + @animation.effect[2].to_i
        @ani_frames = @ani_duration if @ani_frames.nil?
        AnimationEffects.applyeffect(self,@animation.effect[0],(@ani_frames - @ani_duration)+(frame_index - @animation.effect[1].to_i),@animation.effect[2].to_i * @ani_rate)
      end
      if (@animation.effect[3] == "true" and frame_index == @animation.effect[1].to_i + @animation.effect[2].to_i)  and (@ani_duration - 1) % @ani_rate == 0
        AnimationEffects.endeffect(self,@animation.effect[0])
      end
    end
    se_update_animation
  end
  #--------------------------------------------------------------------------
  # * End Animation
  #--------------------------------------------------------------------------
  alias se_end_animation end_animation
  def end_animation
    if @animation && (@animation.effect.size > 0 and !(@animation.effect[3] == "true"))
      AnimationEffects.endeffect(self,@animation.effect[0])
    end
    @ani_frames = nil
    se_end_animation
  end
end

class Sprite_Battler < Sprite_Base
  #--------------------------------------------------------------------------
  # * Update Position
  #--------------------------------------------------------------------------
  alias se_update_position update_position
  def update_position
    return if animation?
    se_update_position
  end
end

class RPG::Animation
  def effect
    if @effect.nil?
      if @name =~ /<effect (.*)>/i
        @effect = $1.split(",")
      else
        @effect = []
      end
    end
    @effect
  end
end