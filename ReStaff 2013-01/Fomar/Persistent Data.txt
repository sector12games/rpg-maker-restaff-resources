=begin
Persistent Data
by Fomar0153
Version 1.0
----------------------
Notes
----------------------
Allows you to preserve the values of selected variables
and switches across all games.
----------------------
Instructions
----------------------
If you wish adjust the strings in the Fomar module.
Then put the PERSISTENT_DATA_STRING in to the names of
the switches or variables that are to be persistent.
E.g.
[p]Talked to mayor
Case 1: Progress [p]
----------------------
Known bugs
----------------------
None
=end

module Fomar
  
  # Enter the string below into the names of the switches or variables
  # that are to be persistent
  PERSISTENT_DATA_STRING = "[p]"
  # DO NOT USE SAVE in the string below
  PFILENAME = "PersistentData.rvdata2"
  
end

class Game_PersistentSwitches
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  def initialize
    @data = {}
  end
  #--------------------------------------------------------------------------
  # * Get Switch
  #--------------------------------------------------------------------------
  def [](switch_id)
    @data[switch_id] || false
  end
  #--------------------------------------------------------------------------
  # * Set Switch
  #     value : ON (true) / OFF (false)
  #--------------------------------------------------------------------------
  def []=(switch_id, value)
    @data[switch_id] = value
    DataManager.save_persistent
  end
end

class Game_PersistentVariables
  #--------------------------------------------------------------------------
  # * Object Initialization
  #--------------------------------------------------------------------------
  def initialize
    @data = {}
  end
  #--------------------------------------------------------------------------
  # * Get Variable
  #--------------------------------------------------------------------------
  def [](variable_id)
    @data[variable_id] || 0
  end
  #--------------------------------------------------------------------------
  # * Set Variable
  #--------------------------------------------------------------------------
  def []=(variable_id, value)
    @data[variable_id] = value
    DataManager.save_persistent
  end
end

class Game_Switches
  #--------------------------------------------------------------------------
  # * Get Switch
  #--------------------------------------------------------------------------
  def [](switch_id)
    if $data_system.switches[switch_id].include?(Fomar::PERSISTENT_DATA_STRING)
      return $game_persistentswitches[switch_id]
    else
      return @data[switch_id] || false
    end
  end
  #--------------------------------------------------------------------------
  # * Set Switch
  #     value : ON (true) / OFF (false)
  #--------------------------------------------------------------------------
  def []=(switch_id, value)
    if $data_system.switches[switch_id].include?(Fomar::PERSISTENT_DATA_STRING)
      $game_persistentswitches[switch_id] = value
    else
      @data[switch_id] = value
    end
    on_change
  end
end

class Game_Variables
  #--------------------------------------------------------------------------
  # * Get Variable
  #--------------------------------------------------------------------------
  def [](variable_id)
    if $data_system.variables[variable_id].include?(Fomar::PERSISTENT_DATA_STRING)
      return $game_persistentvariables[variable_id]
    else
      return @data[variable_id] || 0
    end
  end
  #--------------------------------------------------------------------------
  # * Set Variable
  #--------------------------------------------------------------------------
  def []=(variable_id, value)
    if $data_system.variables[variable_id].include?(Fomar::PERSISTENT_DATA_STRING)
      $game_persistentvariables[variable_id] = value
    else
      @data[variable_id] = value
    end
    on_change
  end
end

module DataManager
  #--------------------------------------------------------------------------
  # * Aliases
  #--------------------------------------------------------------------------
  class << self
    alias persistent_create_game_objects create_game_objects
    alias persistent_load_game load_game
  end
  #--------------------------------------------------------------------------
  # * Execute Load
  #--------------------------------------------------------------------------
  def self.load_game(index)
    self.load_persistent
    return self.persistent_load_game(index)
  end
  #--------------------------------------------------------------------------
  # * Create Game Objects
  #--------------------------------------------------------------------------
  def self.create_game_objects
    self.persistent_create_game_objects
    self.load_persistent
  end
  #--------------------------------------------------------------------------
  # * Save Persistent Data
  #--------------------------------------------------------------------------
  def self.save_persistent
    begin
      File.open(Fomar::PFILENAME, "wb") do |file|
        persistent = {}
        persistent[:switches] = $game_persistentswitches
        persistent[:variables] = $game_persistentvariables
        Marshal.dump(persistent, file)
      end
    rescue
      File.delete(Fomar::PFILENAME) rescue nil
    end
  end
  #--------------------------------------------------------------------------
  # * Load Persistent Data
  #--------------------------------------------------------------------------
  def self.load_persistent
    begin
      File.open(Fomar::PFILENAME, "rb") do |file|
        contents = Marshal.load(file)
        $game_persistentswitches  = contents[:switches]
        $game_persistentvariables = contents[:variables]
      end
    rescue
      $game_persistentswitches  = Game_PersistentSwitches.new
      $game_persistentvariables = Game_PersistentVariables.new
      self.save_persistent
    end
  end
end