# ----------------------------------------------------
# Dr.Yami - Nguyen Tuan Cuong
#
# Introduction:
# This script makes each battler can only affect by one state.
# There is a notetag to make a state an exception, so
# that this state can have along with other state:
# <not single state>
#
# Instruction: 
# Put this script above Main
#
# ----------------------------------------------------

#==============================================================================
# ■ CONFIG
#==============================================================================

module CONFIG
  module SINGLE_STATE
    
    # Set this to true to make new state replace old state.
    REPLACE_STATE = true
    
  end
end # CONFIG

#==============================================================================
# ■ REGEXP
#==============================================================================

module REGEXP
  module SINGLE_STATE
    EXCEPTION = /<not single state>/i
  end
end # REGEXP

#==============================================================================
# ■ DataManager
#==============================================================================

module DataManager
  
  class <<self; alias load_database_mpdec_sist load_database; end
  def self.load_database
    load_database_mpdec_sist
    load_notetags_mpdec_sist
  end
  
  def self.load_notetags_mpdec_sist
    $data_states.each { |obj|
      next if obj.nil?
      obj.load_notetags_mpdec_sist
    }
  end
  
end # DataManager

#==============================================================================
# ■ RPG::BaseItem
#==============================================================================

class RPG::BaseItem
  
  def load_notetags_mpdec_sist
    @not_single_state = false
    self.note.split(/[\r\n]+/).each { |line|
      case line
      when REGEXP::SINGLE_STATE::EXCEPTION
        @not_single_state = true
      end
    }
  end
  
  def not_single_state
    @not_single_state
  end
  
end # RPG::BaseItem

#==============================================================================
# ■ Game_Battler
#==============================================================================

class Game_Battler < Game_BattlerBase
  
  alias luk_effect_rate_mpdec_sist luk_effect_rate
  def luk_effect_rate(user)
    replace_state = CONFIG::SINGLE_STATE::REPLACE_STATE
    if self.states.any? { |s| !s.not_single_state }
      return replace_state ? luk_effect_rate_mpdec_sist(user) : 0.0
    else
      return luk_effect_rate_mpdec_sist(user)
    end
  end
  
  alias add_state_mpdec_sist add_state
  def add_state(state_id)
    replace_state = CONFIG::SINGLE_STATE::REPLACE_STATE
    if state_addable?(state_id) && !self.dead? && replace_state
      self.states.each { |s|
        next if s.not_single_state
        erase_state(s.id)
        refresh
      }
    end
    add_state_mpdec_sist(state_id)
  end
  
end # Game_Battler