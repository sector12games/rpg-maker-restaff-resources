# ----------------------------------------------------
# Dr.Yami - Nguyen Tuan Cuong
#
# Introduction:
# This script provides mime feature for battler, that
# he will have a chance to repeat his last action
# in the same turn.
#
# Notetag:
# <mime action: X%>
# Battler has X% chance to repeat last action. Put in
# actor/enemy notebox or state notebox
#
# <no mime>
# Make that skill cannot be repeated. Put in skill/item
# notebox
#
# Instruction: 
# Put this script above Main
#
# ----------------------------------------------------

#==============================================================================
# ■ REGEXP
#==============================================================================

module REGEXP
  module MIME_ACTION
    MIME_ACTION = /<mime action:[ ]*(\d+)(?:[%％])*>/i
    NO_MIME     = /<no mime>/i
  end
end # REGEXP

#==============================================================================
# ■ DataManager
#==============================================================================

module DataManager
  
  class <<self; alias load_database_mpdec_mime load_database; end
  def self.load_database
    load_database_mpdec_mime
    load_notetags_mpdec_mime
  end
  
  def self.load_notetags_mpdec_mime
    groups = [$data_actors, $data_skills, $data_enemies, $data_items, $data_states]
    groups.each { |group|
      group.each { |obj|
        next if obj.nil?
        obj.load_notetags_mpdec_mime
      }
    }
  end
  
end # DataManager

#==============================================================================
# ■ RPG::BaseItem
#==============================================================================

class RPG::BaseItem
  
  def load_notetags_mpdec_mime
    @mime_action = 0
    @no_mime     = false
    self.note.split(/[\r\n]+/).each { |line|
      case line
      when REGEXP::MIME_ACTION::MIME_ACTION
        @mime_action = [$1.to_i, 100].min
      when REGEXP::MIME_ACTION::NO_MIME
        @no_mime = true
      end
    }
  end
  
  def no_mime
    @no_mime
  end
  
  def mime_action
    @mime_action
  end
  
end # RPG::BaseItem

#==============================================================================
# ■ Game_Action
#==============================================================================

class Game_Action
  
  attr_accessor :mime
  
  alias mpdec_mime_clear clear
  def clear
    mpdec_mime_clear
    @mime = false
  end
  
  def is_mime?
    @mime
  end
  
end # Game_Action

#==============================================================================
# ■ Game_Battler
#==============================================================================

class Game_Battler < Game_BattlerBase
  
  def mime_action
    battler = enemy? ? self.enemy : self.actor
    mime_action = 0
    self.states.each { |s|
      next unless s.mime_action > 0
      mime_action = [mime_action, s.mime_action].max
    }
    mime_action = [mime_action, battler.mime_action].max
    mime_action
  end
  
  def mime_current_action
    battler = enemy? ? self.enemy : self.actor
    return unless mime_action > 0
    return unless current_action
    return unless current_action.item
    return if current_action.item.no_mime
    return if current_action.mime
    return if rand(100) >= mime_action
    mime = current_action.clone
    mime.mime = true
    @actions.insert(1, mime)
  end
  
end # Game_Battler

#==============================================================================
# ■ Scene_Battle
#==============================================================================

class Scene_Battle < Scene_Base
  
  alias mpdec_mime_execute_action execute_action
  def execute_action
    mpdec_mime_execute_action
    return unless @subject
    @subject.mime_current_action
  end
  
end # Scene_Battle