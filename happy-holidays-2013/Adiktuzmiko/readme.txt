============================================================

ReStaff Guest - Adiktuzmiko
2013.12

============================================================

Terms and conditions

SCOPE: This Terms and Conditions are only for this guest resource pack by Adiktuzmiko

This Resource Pack contains a set of guest resources, released as a part of the ReStaff Program. These       resources are compatible with the full version of RPG MAKER VX, RPG Maker XP, and RPG Maker VX Ace and can be    used with IG Maker to create your own game. You may **NOT** use the resources in this pack for creating games    made with engines/programs other than RPG Maker series. 

Also edits of the RTP requires that you have the corresponding Maker from which the RTP was from. 
Any RTP edit will be properly identified via it's filename.

Other information concerning Terms and Conditions can be found here: 

http://lescripts.wordpress.com/terms-and-conditions/

============================================================

