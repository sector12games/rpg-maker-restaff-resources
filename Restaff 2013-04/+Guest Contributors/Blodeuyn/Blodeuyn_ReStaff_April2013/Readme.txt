Name: Blodeuyn
Terms of Use: Contact for Commercial Use, 'Calming Dungeons' is Free for Commercial Use. Credit Required
Contact Info: Kairi Blodeuyn Sawler, PM me on RPGMaker Forums, or E-mail
E-mail: Kairi_Butterfly@hotmail.com
Details: Five Orchestral Tracks, Flawless Looping in .OGG Format, ready for immediate use.
Notes:

TRACKLIST
=========
- Shadows All Around
- Warring Swords
- Swaysea Coast
- Giant's Hill
- Desert Dunes

TERMS
=====
- These five tracks are Free to be used in any Non-Commercial RPGMaker Engine Game. However I require credit to be given to me (D. C. Kairi Sawler).
- If you wish to use these in any commercial game, please send me a PM on the forums (Blodeuyn), or at Kairi_Butterfly@Hotmail.com
- I'd also like a small PM from anyone using the tracks, just so I can keep track of them. Thanks!