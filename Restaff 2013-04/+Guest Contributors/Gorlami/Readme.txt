Name: Gorlami
Terms of Use: Contact me for commercial usage.
Contact Info: kamikaze_watermellon@hotmail.co.uk
Details: Hope's Reach works well as a boss theme. The Eastern Shore is good for some tense/scary buildup. And Sacred Veil is nice for a title song, being all calm and lovely!
Notes: They don't have a set loop points because the songs wrap and loop naturally, but they're still in OGG format.