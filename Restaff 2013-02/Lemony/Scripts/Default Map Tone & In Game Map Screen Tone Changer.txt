#==============================================================================
# ** Lemony's Default Map Tone, (LDMT), (ReStaff Febraury 2013), v.1.0
#==============================================================================
#  I noticed that there is not a built in way of setting default screen tones
# for each map, and making auto start events to set the tone for the map makes
# weird effects. The fact that you can not change the screen tone while
# playtesting makes even more difficult the selection of the right tone for your
# maps.
#  This tool allows the developer to modify the map screen tone while 
# playtesting and to set a default tone for the map. The tone is saved to a file
# in the Data folder in the game directory, and the tone is loaded from it each
# time the map loads. It works in fullscreen mode too.
#==============================================================================
# (*) How to use.-
#==============================================================================
# * To enter or exit the map tone menu, press F5. (Only for Scene_Map)
#
# * When you are ready to distribute your game, set the variable Enabled,
# found in LDMT module, to false.
#
# * For compatibility purposes, the keys used to toggle the tone menu, create 
# the tone file and to reset the tone, can be modified in LDMT module.
#
# * RMVXAce default tone handling mode makes the map tone persist until changed
# by event commands. That makes the tone that has been set in one map, persist
# to the other maps until a new tone is set. If you, for some reason, want to
# avoid that, set to true the variable Reset_T found in LDMT module.
#==============================================================================
# (***) Terms of use.-
#==============================================================================
# This script is free to use in any commercial or non commercial game or 
# project created with any RPG Maker with a valid license as long as explicit
# credits are given to the author (Lemony).
#==============================================================================
module LDMT
  #--------------------------------------------------------------------------
  # * Set to false to disable the map tone menu.
  #--------------------------------------------------------------------------
  Enabled    = true
  #--------------------------------------------------------------------------
  # * Set to true for reseting the map tone when set up. (if no map tone file)
  #--------------------------------------------------------------------------
  Reset_T    = false
  #--------------------------------------------------------------------------
  # * Key for entering & exiting the map tone menu.
  #--------------------------------------------------------------------------
  Toggle_Key = :F5
  #--------------------------------------------------------------------------
  # * Key for creating the map tone file.
  #--------------------------------------------------------------------------
  Create_Key = :F6
  #--------------------------------------------------------------------------
  # * Key for reseting the map tone.
  #--------------------------------------------------------------------------
  Reset_Key  = :CTRL
end
if LDMT::Enabled
  
class LDMT_Set
  #--------------------------------------------------------------------------
  # * Public Instance for disposed.
  #--------------------------------------------------------------------------
  attr_reader :disposed
  #--------------------------------------------------------------------------
  # * LDMT Initialize.
  #--------------------------------------------------------------------------
  def initialize
    @otn = $game_map.screen.tone
    @disposed = false
    @sprite = Sprite.new
    @sprite.z = 99999999
    @sprite.x, @sprite.y = (Graphics.width / 2) - 130, (Graphics.height / 3)
    @sprite.bitmap = Bitmap.new(260, 180)
    font = Font.new('Verdana', 16)
    font.color = Color.new(208, 202, 136)
    font.outline = false
    font.bold = true
    @sprite.bitmap.font = font
    @sprite.bitmap.fill_rect(0, 0, 260, 180, Color.new(58, 58, 58))
    @sprite.bitmap.fill_rect(2, 2, 256, 176, Color.new(208, 202, 136))
    @sprite.bitmap.fill_rect(4, 4, 252, 172, Color.new(58, 58, 58))
    ['Red.', 'Green.', 'Blue.', 'Gray.'].each_with_index {|text, index|
    @sprite.bitmap.draw_text(8 + (61 * index), 36, 61, 16, text, 1)}
    @sprite.bitmap.font.size = 18
    @sprite.bitmap.draw_text(8, 8, 244, 18, "Lemony's Default Map Tone", 1)
    @sprite.bitmap.font.size = 14
    @sprite.bitmap.font.color = Color.new(200, 200, 200)
    resets = "#{LDMT::Reset_Key.to_s} to reset tone values."
    helps0 = "#{LDMT::Create_Key.to_s} to make default map tone."
    helps1 = 'LEFT & RIGHT to select propertie.'
    helps2 = 'UP & DOWN to change values.'
    helps3 = 'Hold SHIFT to speed up.'
    @sprite.bitmap.draw_text(8, 102, 250, 14, resets, 1)
    @sprite.bitmap.draw_text(8, 116, 250, 14, helps0, 1)
    @sprite.bitmap.draw_text(8, 130, 250, 14, helps1, 1)
    @sprite.bitmap.draw_text(8, 144, 250, 14, helps2, 1)
    @sprite.bitmap.draw_text(8, 158, 250, 14, helps3, 1)
    @cursor = Sprite.new
    @cursor.z = @sprite.z + 1
    @cursor.x, @cursor.y = @sprite.x + 8, @sprite.y + 52
    @cursor.bitmap = Bitmap.new(61, 39)
    @cursor.bitmap.fill_rect(0, 0, 61, 39, Color.new(208, 202, 136))
    @cursor.bitmap.clear_rect(2, 2, 57, 35)
    @mindex, t, @font = 0, $game_map.screen.tone, font
    @tindex = [t.red.to_i, t.green.to_i, t.blue.to_i, t.gray.to_i]
    @tindex.each_with_index {|t, i|
    @sprite.bitmap.draw_text(8 + (61 * i), 52, 61, 39, t.to_s, 1)}
  end
  #--------------------------------------------------------------------------
  # * LDMT Dispose.
  #--------------------------------------------------------------------------
  def dispose
    @sprite.bitmap.dispose
    @sprite.dispose
    @cursor.bitmap.dispose
    @sprite.dispose
    @tindex.clear
    @mindex = @tindex = nil
    @disposed = true
  end
  #--------------------------------------------------------------------------
  # * LDMT Draw Tone Value.
  #--------------------------------------------------------------------------
  def draw_tone_value(batch = false)
    x, y = @cursor.x - @sprite.x, @cursor.y - @sprite.y
    if batch
      @tindex.each_with_index {|t, i|
      @sprite.bitmap.fill_rect(8 + (61 * i), 52, 61, 39, Color.new(58, 58, 58))
      @sprite.bitmap.draw_text(8 + (61 * i), 52, 61, 39, t.to_s, 1)}
    else
      @sprite.bitmap.fill_rect(x, y, 61, 39, Color.new(58, 58, 58))
      @sprite.bitmap.draw_text(x, y, 61, 39, @tindex[@mindex], 1)
    end
    $game_map.screen.start_tone_change(Tone.new(*@tindex), 0)
  end
  #--------------------------------------------------------------------------
  # * LDMT Update.
  #--------------------------------------------------------------------------
  def update
    @sprite.update
    @cursor.x, @cursor.y = @sprite.x + 8 + (@mindex * 61), @sprite.y + 52 
    @mindex = [@mindex - 1, 0].max if Input.trigger?(:LEFT)
    @mindex = [@mindex + 1, 3].min if Input.trigger?(:RIGHT)
    speed_up_down = Input.press?(:SHIFT) && Input.press?(:DOWN)
    speed_up_up   = Input.press?(:SHIFT) && Input.press?(:UP)
    if Input.trigger?(LDMT::Create_Key)
      t = $game_map.screen.tone
      str = "#{t.red.to_i} #{t.green.to_i} #{t.blue.to_i} #{t.gray.to_i}"
      string = Zlib::Deflate.deflate(str)
      filenm = "Data/LDMT_MapTones#{$game_map.map_id}.rvdata2"
      File.open(filenm, File::WRONLY|File::TRUNC|File::CREAT|File::BINARY) {|f|
      Marshal.dump(string, f)}
      spr = Sprite.new
      spr.y = 64
      spr.bitmap = Bitmap.new(Graphics.width, 32)
      spr.bitmap.fill_rect(0, 0, Graphics.width, 32, Color.new(0, 0, 0, 199))
      spr.bitmap.font = @font
      spr.opacity = 0
      txt = "Default Map Tone file created successfully."
      spr.bitmap.draw_text(0, 0, Graphics.width, 32, txt, 1)
      39.times {Graphics.update ; spr.opacity += (255 / 39).to_i}
      39.times {Graphics.update ; spr.opacity -= (255 / 39).to_i}
      spr.bitmap.dispose
      spr.dispose
    end
    if Input.trigger?(LDMT::Reset_Key)
      @tindex = [@otn.red.to_i, @otn.green.to_i, @otn.blue.to_i, @otn.gray.to_i]
      draw_tone_value(true)
    end
    if Input.trigger?(:DOWN) || Input.repeat?(:DOWN) || speed_up_down
      @tindex[@mindex] = [@tindex[@mindex] - 1, (@mindex == 3 ? 0 : -255)].max
      draw_tone_value
    elsif Input.trigger?(:UP) || Input.repeat?(:UP)  || speed_up_up
      @tindex[@mindex] = [@tindex[@mindex] + 1, 255].min
      draw_tone_value
    end
  end
end
#==============================================================================
# ** Game_Player
#------------------------------------------------------------------------------
#  Alias to Move by Input, which prevents the player movement when editing tone.
#==============================================================================
class Game_Player < Game_Character
  #--------------------------------------------------------------------------
  # * Alias Move by Input.                                               [MOD]
  #--------------------------------------------------------------------------
  alias ldmt_mbi move_by_input
  def move_by_input
    return if SceneManager.scene.ldmt_set 
    ldmt_mbi
  end
end
#==============================================================================
# ** Scene_Map
#------------------------------------------------------------------------------
#  Aliases update & terminate to handle the tone editor.
#==============================================================================
class Scene_Map < Scene_Base
  #--------------------------------------------------------------------------
  # * Public Instance for LDMT.                                          [NEW]
  #--------------------------------------------------------------------------
  attr_accessor :ldmt_set
  #--------------------------------------------------------------------------
  # * Alias Update.                                                      [MOD]
  #--------------------------------------------------------------------------
  alias ldmt_update update
  def update
    if Input.trigger?(LDMT::Toggle_Key)
      @ldmt_set.dispose        if @ldmt_set
      @ldmt_set = LDMT_Set.new if @ldmt_set.nil?
      @ldmt_set = nil if @ldmt_set.disposed
    end
    @ldmt_set.update if @ldmt_set
    ldmt_update
  end
  #--------------------------------------------------------------------------
  # * Alias Terminate.                                                   [MOD]
  #--------------------------------------------------------------------------
  alias ldmt_terminate terminate
  def terminate
    @ldmt_set.dispose if @ldmt_set
    @ldmt_set = nil   if @ldmt_set
    ldmt_terminate
  end
end

end
#==============================================================================
# ** Game_Map
#------------------------------------------------------------------------------
#  Alias to setup, which allows for automatically using a screen tone.
#==============================================================================
class Game_Map
  #--------------------------------------------------------------------------
  # * Alias Setup.                                                       [MOD]
  #--------------------------------------------------------------------------
  alias ldmt_ini setup
  def setup(*args)
    ldmt_ini(*args)
    tone = load_data("Data/LDMT_MapTones#{@map_id}.rvdata2") rescue nil
    if tone
      data = Zlib::Inflate.inflate(tone).split(' ')
      r, g, b, gr = data[0].to_i, data[1].to_i, data[2].to_i, data[3].to_i
      @screen.start_tone_change(Tone.new(r, g, b, gr), 0)
    end
  end
end
if LDMT::Reset_T
#==============================================================================
# ** Lemony's Reset Map Tone (LRMT)
#------------------------------------------------------------------------------
#  Alias to setup, which allows for reseting the map tone when map changes.
#==============================================================================
class Game_Map
  #--------------------------------------------------------------------------
  # * Alias Setup.                                                       [MOD]
  #--------------------------------------------------------------------------
  alias lrmt_setup setup
  def setup(*args)
    @screen.start_tone_change(Tone.new(0, 0, 0, 0), 0)
    lrmt_setup(*args)
  end
end

end