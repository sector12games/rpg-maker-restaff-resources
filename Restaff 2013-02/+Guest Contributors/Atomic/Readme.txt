Name: Atomic 
Terms of Use: Contact me for commercial usage.
Contact Info: kamikaze_watermellon@hotmail.co.uk
Details: An epic orchestrated boss theme, with a focus on string and brass elements. Was inspired by the famous 'Don't Be Afraid' theme from FF8, and carries the same intense pace. It's called Defiance because I can picture a group of warriors standing up and fighting their greatest enemy in a last-ditch attempt to save the planet from extinction. Or something with equal intensity.
Notes: It doesn't have a set loop point because the song wraps and loops naturally, but it's still in OGG format.