Name: Blodeuyn
Terms of Use: Contact for Commercial Use, Credit Required for all Tracks.
Contact Info: Kairi Blodeuyn Sawler, PM me on RPGMaker Forums, or E-mail
E-mail: Kairi_Butterfly@hotmail.com
Details: Five Orchestral Tracks, Flawless Looping in .OGG Format, ready for immediate use.
Notes:
 
TRACKLIST
=========
- Dreamglass
- Swing Your Pickaxe
- Stretching Time
- Painting the Universe
- Bloomed Dawn
 
TERMS
=====
- These five tracks are Free to be used in any Non-Commercial RPGMaker Engine Game. However I require credit to be given to me (D. C. Kairi Sawler).
- If you wish to use these in any commercial game, please send me a PM on the forums (Blodeuyn), or at Kairi_Butterfly@Hotmail.com