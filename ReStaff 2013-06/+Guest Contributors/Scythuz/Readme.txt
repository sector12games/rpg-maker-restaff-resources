Name: Benjamin Carr
Terms of Use: Contact for commercial use
Contact Info: PM Scythuz or email bc_12@live.co.uk
Details: 3 High quality music tracks for use in traditional fantasy themed rpgs.
Notes: Files are in ogg format to keep file size down, please contact me if you wish to use a different format.