
Name : Gorlami
Terms of Use: Contact me for Commercial Usage.
Contact Info: kamikaze_watermellon@hotmail.co.uk
Details: 6 atmospheric songs and one regular ol' RPG battle theme (Test of Wills).
Notes: Six of them  don't loop, due to the ambience making it difficult to do so without it sounding weird. Test of Wills however loops quite naturally.